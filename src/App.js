import React from "react";
import './App.css';
import SearchBar from "./components/search-bar";

function App() {

  return (
    <div className="App">
      <div id="igv-div" style={{padding: 10}}>
        <SearchBar />
      </div>
    </div>
  );
}

export default App;
