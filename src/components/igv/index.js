import igv from 'igv/dist/igv';
import { useEffect } from "react";

export default function Igv(props) {

    useEffect(() => {
        var igvDiv = document.getElementById("igv-div");
        var options =
        {
            reference: {
                id: props.refGenome.name,
                name: props.refGenome.name,
                fastaURL: `/ref-file/${props.refGenome.id}`,
                indexed: false
            },
            tracks: [
                {
                    type: "alignment",
                    format: "bam",
                    name: "NA12889",
                    url: `/bam/${props.fileName}`,
                    indexURL: `/bam-bai/${props.fileName}`,
                    height: '500'
                }
            ]
        };

        igv.createBrowser(igvDiv, options).then()

        return () => {
            igv.removeAllBrowsers();
        }
    }, [props.refGenome, props.fileName]);
}