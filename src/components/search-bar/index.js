import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Igv from '../igv';

export default function SearchBar() {
    //Search bar states
    const [open, setOpen] = useState(false);
    const [options, setOptions] = useState([]);
    const loading = open && options.length === 0;

    //Igv states
    const [refGenome, setRefGenome] = useState();
    const [fileName, setFileName] = useState();

    useEffect(() => {
        //API call for ref genom list
        if (!loading) {
            return undefined;
        }

        axios.get('/names').then(res => {
            setOptions(res.data)
        })
    }, [loading])


    function getRefGenome(refGenome) {
        axios.get(`/${refGenome.id}`).then(res => {
            setRefGenome(refGenome);
            setFileName(res.data.name)
        })
    }

    return (<><Autocomplete
        id="searchRefGenome"
        sx={{ width: 300, padding: 2 }}
        open={open}
        onOpen={() => {
            setOpen(true);
        }}
        onClose={() => {
            setOpen(false);
        }}
        onChange={(event, value) => {
            getRefGenome(value);
        }}
        options={options}
        loading={loading}
        isOptionEqualToValue={(option, value) => option.name === value.name}
        getOptionLabel={(option) => option.name}
        renderInput={(params) => (
            <TextField
                {...params}
                label="Search reference genomes"
                InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                        <React.Fragment>
                            {loading ? <CircularProgress color="inherit" size={20} /> : null}
                            {params.InputProps.endAdornment}
                        </React.Fragment>
                    ),
                }} />
        )} />{refGenome && fileName && <Igv refGenome={refGenome} fileName={fileName} />}</>)
}